# Store Sales - Time Series Forecasting 

This is a Notebook for the Kaggle competition, Store Sales - Time Series Forecasting: 

https://www.kaggle.com/competitions/store-sales-time-series-forecasting

## Goal

The goal of notebook is to predict sales for the thousands of product families sold at Favorita stores located in Ecuador. The training data includes dates, store and product information, whether that item was being promoted, as well as the sales numbers. 

## Evaluation

The evaluation metric for this competition is Root Mean Squared Logarithmic Error.

## Main Contents

- Linear Regression
- Random Forest Regressor

## Kaggle's notebook:

https://www.kaggle.com/code/mr0024/store-sales-using-lr-and-rf

## Requirements

The requirements file contains all the packages needed to work through this notebook
